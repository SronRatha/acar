<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\User\MailController;
use App\Models\User;

class UserController extends Controller
{
    public function store(UserRequest $request, User $user){
        $user= new User();
        $user->id = sha1(time());
        $user->name = $request->name;
        $user->email= $request->email;
        $user->password = Hash::make( $request->password);
        $user->verification_code = sha1(time());
        //dd($user);
        $user->save();
        
        if($user !=null){
            if($user->is_verified == 0){
                //Send Mail
                MailController::sendSignupEmail($user->name, $user->email,$user->verification_code);
            }       
            //Show message
            return redirect()->back()->with(Session()->flash('alert-success','You have registered successfully. Please check your email to activate the account.'));
        }
        //show message
        return redirect()->back()->with(Session()->flash('alert-warning','Registration was not successful. Please tray again.'));
    }


    public function verifyUser(Request $request){
        $verification_code = \Illuminate\Support\Facades\Request::get('code');
        $user = User::where('verification_code','=',$verification_code)->first();
        if($user !=null){
            $user->is_verified = 1;
            $user->save();

            return redirect()->route('users.login')->with(Session()->flash('alert-success','Your Account was activated. Please login to proceed.'));
        }

        return redirect()->route('users.login')->with(Session()->flash('alert-info','Please check your email to verify'));
    }

    public function handleLogin(Request $request){
        $this->validate($request, [
            'email'   => 'required|email|exists:users,email',
            'password' => 'required|min:5'
        ],[
            'email.exists' => 'This email not exist'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_verified' => 1], $request->get('remember'))) {

            return redirect()->route('users.home');
        }
        return back()->withInput($request->only('email', 'remember'))->with(Session()->flash('alert-danger','Incorrect email and password.'));
    }

    public function home(){
        //return $user;
        $user_detail= User::find(auth()->user()->id)->user_detail()->get();
        //$user_detail= User::find(auth()->user()->id);
        //return $user_detail;
        if($user_detail->isEmpty()){
            //redirect to UserDetail Create
            return view('users.create');
         }
        else{  
            //dd($user)
            $user_detail= User::find(auth()->user()->id);
            return view('users.profile',compact('user_detail'));
        }
    }

    public function logout(Request $request){
        //$this->guard()->logout();
        Auth::logout();
        return redirect()->route('users.login');
    }
}
